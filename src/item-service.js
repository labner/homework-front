class ItemService {

    async retrieveItems()  {
        return fetch("/api/customers")
            .then(response => {
                console.log(response);
                if (!response.ok) {
                    this.handleResponseError(response);
                }
                return response.json();
            })
            .then(json => {
                console.log("Retrieved items:");
                console.log(json);
                return json;
            })
            .catch(error => {
                this.handleError(error);
            });
    }
    async getItem(item) {
        return(item);

    }
    async createItem(newitem) {
        console.log("ItemService.createItem():");
        console.log(newitem);
        return fetch("/api/customers", {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(newitem)
        })
            .then(response => {
                if (!response.ok) {
                    this.handleResponseError(response);
                }
                return response.json();
            })
            .catch(error => {
                this.handleError(error);
            });
    }
    async deleteItem(id) {
        console.log("ItemService.deleteItem():");
        console.log("item: " + id);
        return fetch("/api/customers?id=" + id, {
            method: "DELETE",
            mode: "cors"
        })
            .then(response => {
                if (!response.ok) {
                    this.handleResponseError(response);
                }
            })
            .catch(error => {
                this.handleError(error);
            });
    }
    async updateItem(item) {
        console.log("ItemService.updateItem():");
        console.log(item);
        return fetch("/api/customers", {
            method: "PUT",
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(item)
        })
            .then(response => {
                if (!response.ok) {
                    this.handleResponseError(response);
                }
                return response.json();
            })
            .catch(error => {
                this.handleError(error);
            });
    }
    handleResponseError(response) {
        throw new Error("HTTP error, status = " + response.status);
    }
    handleError(error) {
        console.log(error.message);
    }
}
export default ItemService;
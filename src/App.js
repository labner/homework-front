import React, { Component } from 'react';
import './App.css';
import NewItem from './new-item';
import EditItem from './edit-item';
import ItemService from './item-service';
import Autocomplete from './autocomplete';

class App extends Component {
    constructor(props) {
        super(props);
        this.itemService = new ItemService();
        this.onNewItem = this.onNewItem.bind(this);
        this.onEditItem = this.onEditItem.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onCreateItem = this.onCreateItem.bind(this);
        this.onUpdateItem = this.onUpdateItem.bind(this);
        this.onDeleteItem = this.onDeleteItem.bind(this);
        this.onSort = this.onSort.bind(this);
        this.state = {
            editItem: false,
            selectedItem: null,
            newItem: null
        }
    }
    componentDidMount() {
        this.getItems();
    }
    render() {
        const items = this.state.items;
        if(!items) return null;
        const selectedItem = this.state.selectedItem;
        const newItem = this.state.newItem;
        const editItem = this.state.editItem;
        const listItems = items.map((item) =>
            <tr key={item.id}>
                <td>{item.id}</td><td>{item.firstName}</td><td>{item.lastName}</td><td>{item.dob}</td><td>{item.email}</td><td><button onClick={() => this.onDeleteItem(item.id)}>Delete</button>
                <button onClick={() => this.onEditItem(item)}>Edit</button></td>
            </tr>);
        console.log("render " + items);

        return (
            <div className="App">
                <img src="EE_logo.png" alt="EE logo"/>
                <div className="namebox">Homework by Liina Abner</div>
                <div>
                    <br/>
                     <button className="newbutton" type="button" name="button" onClick={() => this.onNewItem()}>New Customer</button>
                    {newItem && <NewItem onSubmit={this.onCreateItem} onCancel={this.onCancel}/>}
                    {editItem && selectedItem && <EditItem onSubmit={this.onUpdateItem} onCancel={this.onCancel} item={selectedItem} />}
                </div>
                <br/>

                <Autocomplete items={items}/>
                <table>
                    <thead>
                    <tr>
                        <th onClick={e => this.onSort(e, 'id')}>Id &darr;</th><th onClick={e => this.onSort(e, 'firstName')}>First Name &darr;</th><th onClick={e => this.onSort(e, 'lastName')}>Last Name &darr;</th><th onClick={e => this.onSort(e, 'dob')}>Date Of Birth &darr;</th><th onClick={e => this.onSort(e, 'email')}>Email &darr;</th><th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        {listItems}
                    </tbody>
                </table>
            </div>
        );
    }

    getItems() {
        this.itemService.retrieveItems().then(items => {
                this.setState({items: items});
            }
        );
    }
    onCancel() {
        this.clearState();
    }
    onNewItem() {
        this.clearState();
        this.setState({
            newItem: true
        });
    }
    onEditItem(item) {
        this.setState({
            editItem: true,
            newItem: null,
            selectedItem: item
        });
    }

    onUpdateItem(item) {
        this.clearState();
        this.itemService.updateItem(item).then(item => {
                this.getItems();
            }
        );
    }
    onCreateItem(newItem) {
        this.clearState();
        this.itemService.createItem(newItem).then(item => {
                this.getItems();
            }
        );
    }
    onDeleteItem(itemId) {
        this.clearState();
        this.itemService.deleteItem(itemId).then(res => {
                this.getItems();
            }
        );
    }
    clearState() {
        this.setState({
            selectedItem: null,
            editItem: false,
            newItem: null
        });
    }

    onSort(event, sortKey){
        const items = this.state.items;
        if(sortKey === 'id') {
            console.log(sortKey);
            items.sort((a,b) => a[sortKey]-b[sortKey])
        } else {
            items.sort((a, b) => a[sortKey].localeCompare(b[sortKey]))
        }
        this.setState({items})
    }
}
export default App;
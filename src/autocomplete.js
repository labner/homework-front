//Compliments to https://github.com/moroshko/react-autosuggest

import Autosuggest from 'react-autosuggest';
import React, { Component } from 'react';

class Autocomplete extends Component {
    constructor(props) {
        super(props);
        this.getSuggestions = this.getSuggestions.bind(this);
        this.state = {
            value: '',
            suggestions: []
        };
    }

   getSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : this.props.items.filter(item =>
            item.firstName.toLowerCase().slice(0, inputLength) === inputValue
            || item.lastName.toLowerCase().slice(0, inputLength) === inputValue
        );
    };

    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        });
    };

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const getSuggestionValue = suggestion => (suggestion.firstName + ' ' + suggestion.lastName);
        const renderSuggestion = suggestion => (
            <div>
                {suggestion.firstName}&nbsp;{suggestion.lastName}
            </div>
        );
        const { value, suggestions } = this.state;

        const inputProps = {
            placeholder: 'Search',
            value,
            onChange: this.onChange
        };

        return (
            <Autosuggest
                suggestions={suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={inputProps}
            />
        );
    }
}
export default Autocomplete;
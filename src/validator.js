class Validator {
    validateInputs(inputData) {
        let errorMsg = "";
        if(!inputData.firstName) {
            errorMsg +="Please enter first name.\n"
        }

        if(!inputData.lastName) {
            errorMsg +="Please enter last name.\n"
        }

        if(!inputData.dob.toString().match(/^\d{4}-\d{2}-\d{2}$/)) {
            errorMsg +="DOB must be yyyy-mm-dd.\n"
        }

        //https://emailregex.com/
        if(!inputData.email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            errorMsg +="Email must match https://emailregex.com/.\n"
        }
        if(errorMsg.length === 0){
            return true;
        } else {
            alert(errorMsg);
            return false;
        }
    }
}
export default Validator;
import React, { Component } from 'react';
import './App.css';
import Validator from './validator';
class EditItem extends Component {
    constructor(props) {
        super(props);
        this.validator = new Validator();
        this.onCancel = this.onCancel.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        const itemToEdit = this.props.item;
        this.state = {
            id: itemToEdit.id,
            firstName: itemToEdit.firstName,
            lastName: itemToEdit.lastName,
            dob: itemToEdit.dob,
            email: itemToEdit.email,
        };
    }
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    onCancel() {
        this.props.onCancel();
    }
    onSubmit() {
        if (this.validator.validateInputs(this.state)) {
            this.props.onSubmit(this.state);
        }
    }
    render() {
        return (
            <div className="input-panel">
                <div>
                   <label className="field-name">Customer Id: </label>
                    {this.state.id}
                </div>
                <div>
                    <label className="field-name">First name:</label>
                    <input value={this.state.firstName} name="firstName" maxLength="40" required onChange={this.handleInputChange} placeholder="First Name" />

                </div>
                <div>
                    <label className="field-name">Last Name:</label>
                     <input value={this.state.lastName} name="lastName" maxLength="40" required onChange={this.handleInputChange} placeholder="Last Name" />
                </div>
                <div>
                    <label className="field-name">Date of Birth:</label>
                    <input value={this.state.dob} name="dob" maxLength="10" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" onChange={this.handleInputChange} placeholder="yyyy-mm-dd" />
                </div>
                <div>
                    <label className="field-name">Email:</label>
                    <input value={this.state.email} name="email" onChange={this.handleInputChange} placeholder="email" />
                </div>
                <label className="field-name"></label>
                <button onClick={() => this.onCancel()}>Cancel</button>
                <button onClick={() => this.onSubmit()}>Update</button>
            </div>
        );
    }
}
export default EditItem;